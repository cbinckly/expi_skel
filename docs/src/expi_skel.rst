expi_skel
---------

This page contains the auto-generated documentation from 
the included :ref:`expi_skel/__init__.py` file.

.. automodule:: expi_skel
    :members:
    :undoc-members:
    :show-inheritance:

