"""
expi_skel package root.

This file contains code that is run when the package is imported.
Place two things in here:

- constants, functions, classes that are required throughout the
  package;
- anything that you want to be able to import rom the package root::

    from expi_skel import ExpiSkel
"""

class ExpiSkelError(RuntimeError):
    """A custom exception that includes the string that was input.

    :params message: exception message.
    :type message: str
    :param string: string causing the error.
    :type string: str
    :returns: :ref:`expi_skel.ExpiSkelError`
    """
    def __init__(self, message, string):
        super(ExpiSkelError(self)).__init__(message)
        self.string = string


class ExpiSkel(str):
    """A class to demonstrate auto-documentation of python code.

    This class will be included in the auto-generated documentation
    and the docstrings (like this one) will automatically be rendered
    and made available along with the package.

    Document the function parameters, return, and exception information
    using the special keywords::

        :param string: the string to make an ExpiSkel instance of.
        :type string: str
        :rtype: :ref:`expi_skel.ExpiSkel`
        :returns: new instance
        :raises: ExpiSkelError if overlength
        :raises: TypeError if string is not an ``str`` object


    :param string: the string to make an ExpiSkel instance of.
    :type string: str
    :rtype: :ref:`expi_skel.ExpiSkel`
    :returns: new instance
    :raises: ExpiSkelError if overlength
    :raises: TypeError if string is not an ``str`` object
    """

    MAX_LENGTH = 128
    """Constants can also be documented by adding a docstring or comment
    immediately after their definition. The maximum length of the string."""

    def __init__(self, string):

        if string and isinstance(string, str):
            if len(string) > self.MAX_LENGTH:
                raise ExpiSkelError("Overlength!", string)
        else:
            raise TypeError("ExpiSkel wraps str objects only.", string)

        self.string = string

    def first_character(self, string):
        """Get the first character.

        :returns: first character of string or an empty string.
        :rtype: str
        """
        if len(self.string):
            return self.string[0]
        return ""
