=======================
expi skel
=======================

-----------------------------------------------
Package Skeleton for the Extender Package Index
-----------------------------------------------

The ``expi_skel`` package provides a template for creating packages 
for the `Extender Package Index`_ (``expi``).  In addition to preconfigured
standard Python packaging files, such as ``setup.py`` and ``MANIFEST.in``,
the skeleton contains a sample ``expi.json`` file and ``vi/`` directory.

Create a Package - Quickstart
*****************************

To create your own package, export the `expi_skel repository`_ and use it as a 
template.  If your package will depend on packages from `pypi`_, or just as a
general best practice for all Python projects, start by creating and activating
a new virtual environment::

    python3.4 -m venv ~/env3/package_name
    . ~/env3/package_name/bin/activate

.. _expi_skel repository: https://bitbucket.org/cbinckly/expi_skel
.. _pypi: https://pypi.org

Navigate to your projects directory, clone the ``expi_skel`` repository into
a new directory with the package name, and rename the internal package 
directory::
    
    git clone https://bitbucket.org/cbinckly/expi_skel.git my_package_name
    mv my_package_name/expi_skel my_package_name/my_package_name

Customize the package by editing the following files:

- ``setup.py``: change the package name, version, author, dependencies
- ``expi.json``: update the ``expi`` metadata for the package
- ``README.rst`` and ``docs/index.rst``: add your own documentation
- ``LICENSE.txt``: update the licensing terms

If the package will contain an extender module, rename and edit the 
sample module file::

    mv my_package_name/my_package_name/vi/EXPISKEL.vi my_package_name/my_package_name/vi/MYMODULE.company.vi

If the package will not contain a module, remove the sample module
file::

    rm my_package_name/my_package_name/vi/EXPISKEL.vi

Before releasing, be sure to review the following files to make sure everything
that you need will be included in the package:

- ``MANIFEST.in`` - include any additional files in the distribution root.
- ``setup.py:package_data`` - include any additional files in the package root.

Extender Metadata
*****************

The ``expi.json`` file contains additional metadata about the package, such
as compatible Sage 300 and Orchid Extender versions, which Sage Views and
Screens are involved, and information on visibility and licensing.

The additional metadata enables ``expi`` to automatically generate package
landing pages and selectively serve packages only to authorized users. It
enables the `Python Package Manager for Extender`_ to perform
compatiblity checking so customizations are only ever installed on compatible 
systems.

.. _Extender Package Index: https://expi.2665093.ca
.. _Python Package Manager for Extender: https://2665093.ca/#extender-package-manager

The ``vi`` directory may contain Extender modules, ``*.vi`` files, that 
will be registered with Extender automatically after the Python package 
installation has completed.


